<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('customers')->insert([
          'name' => Str::random(10),
          'address' => Str::random(50),
          'email' => Str::random(10).'@gmail.com',
          'phone' => mt_rand(1000000000, 9999999999),
      ]);
    }
}
