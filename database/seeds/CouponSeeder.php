<?php

use Illuminate\Database\Seeder;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $i = 1;
      $discount = array('50'=>'15','100'=>'12','200'=>'10','500'=>'8','1000'=>'5','2000'=>'4','5000'=>'2','10000'=>'1');
        foreach ($discount as $key => $value) {
          DB::table('coupons')->insert(array(
            array(
             'discount_value' => 'Rs'.$key,
             'no_of_times' => $value,
             'probability' => $i++,
            ),
          ));
        }
    }
}
