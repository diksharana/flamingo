<?php

use Illuminate\Database\Seeder;

class Customer_Coupon_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('customer_coupon')->insert([
          'customer_id' => '1',
          'coupon_id' => '1',
      ]);
    }
}
