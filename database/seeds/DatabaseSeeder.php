<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call('CustomerSeeder');
      $this->call('CouponSeeder');
      $this->call('Customer_Coupon_Seeder');

      $this->command->info('Customer table, Coupon table and Customer_Coupon Table seeded!');
    }
}
