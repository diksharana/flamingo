<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_Coupon extends Model
{
    protected $table = 'customer_coupon';
    protected $fillable = ['customer_id','coupon_id'];
}
