<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Customer;
use App\Coupon;
use App\Customer_Coupon;
use DB;

class CustomerController extends Controller
{
  public function customer(Request $request)
  {
    $validator = \Validator::make($request->all(), [
      'name'  => 'required',
      'email'  => 'required',
      'phone'  => 'required',
      'address'  => 'required'
    ]);
    if ($validator->fails()) {
       return response()->json($validator->errors(), 422);
    }
    $all = $request->all();
    $customer = new Customer;
    try {
        $values = Coupon::where('no_of_times','>',0)->pluck('no_of_times','discount_value')->toArray();
        // all coupons used, no discount given but customer added succesfully
        if (Coupon::get()->sum('no_of_times') == 0) {
          $customer_save = Customer::Create($all);
           return response()->json(['message'=>'Customer added succesfully, Discount coupons over']);
        }
        $newitems = [];
          foreach ($values as $times=>$value) {
            for ($i=0; $i < $value; $i++) {
              $newitems[] = $times;
            }
          }
        $discount = $newitems[array_rand($newitems)];
        $customer_save = Customer::Create($all); //customer created succesfully
        $id = Coupon::where('discount_value', '=', $discount)->pluck('id')->toArray();
        $coupon_save = Coupon::where('discount_value', '=', $discount)->update(['no_of_times' =>$values[$discount]-1]);
        $customer_coupon_save = DB::table('customer_coupon')->insert(['coupon_id'=>$id[0],'customer_id'=>$customer_save->id]);
        return response()->json(['message'=>'Customer added succesfully and Coupon applied with discount '.$discount]);
    }
    catch (QueryException $e) {
      $errorCode = $e->errorInfo[1];
      if($errorCode == 1062){
          return response()->json(['message'=>'Given phone number or email address has been used previously']);
      }
    }
  }
}
